/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.distributed;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;


public interface DistributedSupport {

    
    <K, V> ConcurrentMap<K, V> getMap(String namespace);

    
    <T> List<T> getList(String namespace);

    
    <T> Set<T> getSet(String namespace);
}
